package br.com.torresti.musica.util.dto;

public class MusicaDiscoDTO {

	private String artista;

	private String musica;
	
	public MusicaDiscoDTO() {}
	
	public MusicaDiscoDTO(String artista, String musica) {
		this.artista = artista;
		this.musica = musica;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public String getMusica() {
		return musica;
	}

	public void setMusica(String musica) {
		this.musica = musica;
	}
	
}

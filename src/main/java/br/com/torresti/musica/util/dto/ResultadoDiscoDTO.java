package br.com.torresti.musica.util.dto;

import java.util.ArrayList;
import java.util.List;

public class ResultadoDiscoDTO {

	private List<DiscoDTO> listaDiscoMenorDTO;

	private String nomePrateleira = "Sem Prateleira";
	
	private Integer posicaoPrateleira = 0;
	
	private Integer totalPrateleira = 0;
	
	private DiscoDTO discoDTO;

	private List<DiscoDTO> listaDiscoMaiorDTO;

	public ResultadoDiscoDTO() {
		this.listaDiscoMenorDTO = new ArrayList<>();
		this.listaDiscoMaiorDTO = new ArrayList<>();
	}

	public List<DiscoDTO> getListaDiscoMenorDTO() {
		return listaDiscoMenorDTO;
	}

	public void setListaDiscoMenorDTO(List<DiscoDTO> listaDiscoMenorDTO) {
		this.listaDiscoMenorDTO = listaDiscoMenorDTO;
	}
	
	public String getNomePrateleira() {
		return nomePrateleira;
	}

	public void setNomePrateleira(String nomePrateleira) {
		this.nomePrateleira = nomePrateleira;
	}

	public Integer getPosicaoPrateleira() {
		return posicaoPrateleira;
	}

	public void setPosicaoPrateleira(Integer posicaoPrateleira) {
		this.posicaoPrateleira = posicaoPrateleira;
	}

	public Integer getTotalPrateleira() {
		return totalPrateleira;
	}

	public void setTotalPrateleira(Integer totalPrateleira) {
		this.totalPrateleira = totalPrateleira;
	}

	public DiscoDTO getDiscoDTO() {
		return discoDTO;
	}

	public void setDiscoDTO(DiscoDTO discoDTO) {
		this.discoDTO = discoDTO;
	}

	public List<DiscoDTO> getListaDiscoMaiorDTO() {
		return listaDiscoMaiorDTO;
	}

	public void setListaDiscoMaiorDTO(List<DiscoDTO> listaDiscoMaiorDTO) {
		this.listaDiscoMaiorDTO = listaDiscoMaiorDTO;
	}

}

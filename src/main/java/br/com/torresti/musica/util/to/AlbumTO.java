package br.com.torresti.musica.util.to;

import java.util.ArrayList;
import java.util.List;

import br.com.torresti.spring.to.RestTO;


public class AlbumTO extends RestTO {

	private String selo;

	private String ano;

	private String album;

	private String urlImagem;

	private List<ArtistaMusicaVO> listaArtistaMusicaTO = new ArrayList<ArtistaMusicaVO>();

	public String getSelo() {
		return selo;
	}

	public void setSelo(String selo) {
		this.selo = selo;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

	public List<ArtistaMusicaVO> getListaArtistaMusicaTO() {
		return listaArtistaMusicaTO;
	}

	public void setListaArtistaMusicaTO(List<ArtistaMusicaVO> listaArtistaMusicaTO) {
		this.listaArtistaMusicaTO = listaArtistaMusicaTO;
	}

}

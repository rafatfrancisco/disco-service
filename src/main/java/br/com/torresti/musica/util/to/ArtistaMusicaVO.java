package br.com.torresti.musica.util.to;

public class ArtistaMusicaVO {

	private String artista;

	private String musica;
	
	public ArtistaMusicaVO() {}
	
	public ArtistaMusicaVO(String artista, String musica) {
		this.artista = artista;
		this.musica = musica;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public String getMusica() {
		return musica;
	}

	public void setMusica(String musica) {
		this.musica = musica;
	}
	
}

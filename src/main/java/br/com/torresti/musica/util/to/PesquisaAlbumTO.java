package br.com.torresti.musica.util.to;

import java.util.Map;

import br.com.torresti.spring.to.RestTO;

public class PesquisaAlbumTO extends RestTO {

	private Map<Integer, AlbumTO> mapAlbumTO;
	
	public Map<Integer, AlbumTO> getMapAlbumTO() {
		return mapAlbumTO;
	}

	public void setMapAlbumTO(Map<Integer, AlbumTO> mapAlbumTO) {
		this.mapAlbumTO = mapAlbumTO;
	}

}

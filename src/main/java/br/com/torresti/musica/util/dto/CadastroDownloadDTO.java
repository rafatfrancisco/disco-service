package br.com.torresti.musica.util.dto;

import java.util.List;

public class CadastroDownloadDTO {

	private Integer tipo;

	private List<String> listaUrl;

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public List<String> getListaUrl() {
		return listaUrl;
	}

	public void setListaUrl(List<String> listaUrl) {
		this.listaUrl = listaUrl;
	}

}

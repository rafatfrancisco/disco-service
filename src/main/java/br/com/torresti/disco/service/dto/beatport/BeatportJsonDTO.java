package br.com.torresti.disco.service.dto.beatport;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeatportJsonDTO {

	@JsonProperty(value = "artists")
	private List<BeatportArtistaJsonDTO> artists;

	@JsonProperty(value = "date")
	private BeatportDateJsonDTO date;

	@JsonProperty(value = "images")
	private BeatportImagemJsonDTO images;

	@JsonProperty(value = "label")
	private BeatportSeloJsonDTO label;

	@JsonProperty(value = "name")
	private String name;

	public List<BeatportArtistaJsonDTO> getArtists() {
		return artists;
	}

	public void setArtists(List<BeatportArtistaJsonDTO> artists) {
		this.artists = artists;
	}

	public BeatportDateJsonDTO getDate() {
		return date;
	}

	public void setDate(BeatportDateJsonDTO date) {
		this.date = date;
	}

	public BeatportImagemJsonDTO getImages() {
		return images;
	}

	public void setImages(BeatportImagemJsonDTO images) {
		this.images = images;
	}

	public BeatportSeloJsonDTO getLabel() {
		return label;
	}

	public void setLabel(BeatportSeloJsonDTO label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

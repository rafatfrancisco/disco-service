package br.com.torresti.disco.service.dto.beatport;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeatportImagemJsonDTO {

	@JsonProperty(value = "large")
	private BeatportLargeImagemJsonDTO large;

	public BeatportLargeImagemJsonDTO getLarge() {
		return large;
	}

	public void setLarge(BeatportLargeImagemJsonDTO large) {
		this.large = large;
	}

}

package br.com.torresti.disco.service.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.util.ArtistaMusicaUtil;
import br.com.torresti.musica.util.StringUtil;
import br.com.torresti.musica.util.to.AlbumTO;
import br.com.torresti.musica.util.to.ArtistaMusicaVO;
import br.com.torresti.spring.exception.BusinessException;

@SuppressWarnings("deprecation")
@Component
public class JunoComponent extends BaseComponent {
		
	public AlbumTO consultarRelease(String query) throws BusinessException {
		
		String url = "https://www.juno.co.uk/search/?q[all][]=" + query + "&solrorder=relevancy&hide_forthcoming=0&media_type=vinyl&show_out_of_stock=1&facet[subgenre_id][]=15||4&facet[subgenre_id][]=12||3&facet[subgenre_id][]=15||6&facet[subgenre_id][]=15||9&facet[subgenre_id][]=15||99&facet[subgenre_id][]=15||3&facet[subgenre_id][]=14||99&facet[subgenre_id][]=30||1&facet[subgenre_id][]=30||2";
		
		Document document = this.getJsoup(url);
		
		Elements elements = document.select(".product-list");
		
		if (elements == null || elements.isEmpty()) { 
		
			if (elements == null || elements.isEmpty()) { 
			
				throw new BusinessException("Não encontrou a query  " + query + " na Juno.");
				
			}
			
		}
				
		for (Element resultado : elements) {
			
			String idJuno = resultado.select(".dv-item").attr("id");
			
			if (!StringUtil.isEmpty(idJuno)) {
				
				idJuno = idJuno.replace("item-", "");
				
				AlbumTO albumTO = consultarReleaseId(idJuno);
				
				if (albumTO != null) {
					
					return albumTO;
					
				} else {
					
					throw new BusinessException("Não encontrou o release com as informações de idJuno " + idJuno + " na Juno.");
					
				}
				
				
			} else {
				
				throw new BusinessException("Não encontrou as informações de de idJuno " + idJuno + " na Juno.");
				
			}
			
		}
				
		throw new BusinessException("Erro consulta no Beatport.");
		
	}

	public AlbumTO consultarReleaseId(String idJuno) throws BusinessException {
		
		AlbumTO albumTO = null;
		
		String url = "https://www.juno.co.uk/products/" + idJuno + "/";
		
		Document document = this.getJsoup(url);
		
		if (document != null) {
			
			albumTO = new AlbumTO();
			
			String artista = ArtistaMusicaUtil.ajustarArtista(document.select(".product-artist").select("h2").select("a").text());
			
			albumTO.setAlbum(ArtistaMusicaUtil.ajustarAlbum(document.select(".product-title").select("h2").select("span").text()));
						
			String ano = document.select(".product-meta").select("meta").first().attr("content");
			
			albumTO.setAno(ano.split(" ")[ano.split(" ").length - 1]);
			
			albumTO.setSelo(ArtistaMusicaUtil.ajustarSelo(document.select(".product-label").select("h2").select("a").text()));
						
			String urlImagem = document.select(".thickbox").attr("href");
			
			albumTO.setUrlImagem(urlImagem);
						
			Elements lista = document.select(".table_light");
			
			List<ArtistaMusicaVO> listaArtistaMusicaTO = new ArrayList<>();
			
			for (Element element : lista) {
				
				for (Element elementMusica : element.select("tbody").select("tr")) {
					
					String musica = elementMusica.select("td").last().html();
					
					musica = StringEscapeUtils.unescapeHtml4(musica);
					
					if (musica.indexOf("-") != -1) {
					
						artista = ArtistaMusicaUtil.ajustarArtista(musica.split("-")[0].trim());
					
						musica = musica.split("-")[1].trim();
						
					}
					
					musica = ArtistaMusicaUtil.ajustarMusica(StringUtil.removerLast(musica.replace("\"", ""), "(", ")"), false);
					
					ArtistaMusicaVO artistaMusicaTO = new ArtistaMusicaVO();
					
					artistaMusicaTO.setArtista(artista);
					
					artistaMusicaTO.setMusica(musica);
					
					listaArtistaMusicaTO.add(artistaMusicaTO);
					
					
				}
				
				
			}
			
			albumTO.setListaArtistaMusicaTO(listaArtistaMusicaTO);
			
		}
		
		return albumTO;
		
	}
	
}

package br.com.torresti.disco.service.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.torresti.disco.service.component.BeatportComponent;
import br.com.torresti.disco.service.component.DecksComponent;
import br.com.torresti.disco.service.component.DeejayDeComponent;
import br.com.torresti.disco.service.component.DiscogsComponent;
import br.com.torresti.disco.service.component.JunoComponent;
import br.com.torresti.musica.util.MusicaConstantes.TipoSite;
import br.com.torresti.musica.util.to.AlbumTO;
import br.com.torresti.musica.util.to.PesquisaAlbumTO;
import br.com.torresti.spring.exception.BusinessException;
import br.com.torresti.spring.util.SituacaoRest;

@Component
public class LojaService {

	@Autowired
	private JunoComponent junoComponent;
	
	@Autowired
	private DiscogsComponent discogsComponent;
	
	@Autowired
	private BeatportComponent beatportComponent;
	
	@Autowired
	private DecksComponent decksComponent;
	
	@Autowired
	private DeejayDeComponent deejayDeComponent;
	
	public PesquisaAlbumTO consultarQuery(String query) throws Exception {
		
		PesquisaAlbumTO pesquisaAlbumTO = new PesquisaAlbumTO();
		pesquisaAlbumTO.setMapAlbumTO(new HashMap<Integer, AlbumTO>());
		pesquisaAlbumTO.getMapAlbumTO().put(TipoSite.Juno.getTipo(), this.consultarQuery(TipoSite.Juno.getTipo(), query));
		pesquisaAlbumTO.getMapAlbumTO().put(TipoSite.Discogs.getTipo(), this.consultarQuery(TipoSite.Discogs.getTipo(), query));
		pesquisaAlbumTO.getMapAlbumTO().put(TipoSite.Beatport.getTipo(), this.consultarQuery(TipoSite.Beatport.getTipo(), query));
		pesquisaAlbumTO.getMapAlbumTO().put(TipoSite.Decks.getTipo(), this.consultarQuery(TipoSite.Decks.getTipo(), query));
		pesquisaAlbumTO.getMapAlbumTO().put(TipoSite.DeejayDe.getTipo(), this.consultarQuery(TipoSite.DeejayDe.getTipo(), query));
		
		return pesquisaAlbumTO;

	}
	
	public AlbumTO consultarQuery(Integer tipo, String query) throws Exception {
		
		try {
			
			if (TipoSite.Juno.getTipo() == tipo) {
				return junoComponent.consultarRelease(query);
			}
			
			if (TipoSite.Discogs.getTipo() == tipo) {
				return discogsComponent.consultarRelease(query);
			}

			if (TipoSite.Beatport.getTipo() == tipo) {
				return beatportComponent.consultarRelease(query);
			}

			if (TipoSite.Decks.getTipo() == tipo) {
				return decksComponent.consultarRelease(query);
			}
			
			if (TipoSite.DeejayDe.getTipo() == tipo) {
				return deejayDeComponent.consultarRelease(query);
			}

			throw new BusinessException("Tipo inválido!");
			
		} catch (Exception e) {
			
			AlbumTO albumTO = new AlbumTO();
			albumTO.setCodigo(SituacaoRest.Erro.getCodigo());
			albumTO.setMensagem(e.getMessage());
			return albumTO;
			
		}
		
	}
	
	public AlbumTO consultarId(Integer tipo, String id) throws Exception {
		
		if (TipoSite.Juno.getTipo() == tipo) {
			return junoComponent.consultarReleaseId(id);
		}

		if (TipoSite.Discogs.getTipo() == tipo) {
			return discogsComponent.consultarReleaseId(id);
		}
		
		if (TipoSite.Decks.getTipo() == tipo) {
			return decksComponent.consultarReleaseId(id);
		}
		
		if (TipoSite.DeejayDe.getTipo() == tipo) {
			return deejayDeComponent.consultarReleaseId(id);
		}

		throw new BusinessException("Tipo inválido!");
		
	}

}

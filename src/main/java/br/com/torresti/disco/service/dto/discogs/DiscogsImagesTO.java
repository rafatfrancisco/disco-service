package br.com.torresti.disco.service.dto.discogs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class DiscogsImagesTO.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscogsImagesTO {
	
	/** The uri. */
	@JsonProperty(value = "uri")
	private String uri;

	/**
	 * Gets the uri.
	 *
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * Sets the uri.
	 *
	 * @param uri the new uri
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	
}

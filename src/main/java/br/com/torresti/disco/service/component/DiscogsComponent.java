package br.com.torresti.disco.service.component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.torresti.disco.service.dto.discogs.DiscogsArtistsTO;
import br.com.torresti.disco.service.dto.discogs.DiscogsReleaseTO;
import br.com.torresti.disco.service.dto.discogs.DiscogsResultReleaseTO;
import br.com.torresti.disco.service.dto.discogs.DiscogsSearchReleaseTO;
import br.com.torresti.disco.service.dto.discogs.DiscogsTracklistTO;
import br.com.torresti.disco.service.util.DiscogsConstantes;
import br.com.torresti.musica.util.DecksUtil;
import br.com.torresti.musica.util.HtmlUtil;
import br.com.torresti.musica.util.to.AlbumTO;
import br.com.torresti.musica.util.to.ArtistaMusicaVO;
import br.com.torresti.spring.exception.BusinessException;

@Component
public class DiscogsComponent {
	
	private String carregarKeySecret() {

		return "key=" + DiscogsConstantes.discogsKey + "&secret=" + DiscogsConstantes.discogsSecret;

	}

	private DiscogsSearchReleaseTO listarRelease(String query) throws BusinessException {

		String url = "https://api.discogs.com/database/search?q=" + query + "&" + this.carregarKeySecret();

		String json = HtmlUtil.getHttpURLConnection(url);

		DiscogsSearchReleaseTO discogsSearchReleaseTO = new DiscogsSearchReleaseTO();

		Gson gson = new Gson();

		gson.toJson(discogsSearchReleaseTO);

		return gson.fromJson(json, new TypeToken<DiscogsSearchReleaseTO>() {}.getType());

	}

	private DiscogsReleaseTO consultarIdRelease(String idDiscogs) throws BusinessException {

		String url = "https://api.discogs.com/releases/" + idDiscogs + "?" + this.carregarKeySecret();

		String json = HtmlUtil.getHttpURLConnection(url);

		Gson gson = new Gson();

		DiscogsReleaseTO discogsReleaseTO = new DiscogsReleaseTO();

		gson.toJson(discogsReleaseTO);

		return gson.fromJson(json, new TypeToken<DiscogsReleaseTO>() {
		}.getType());

	}

	public AlbumTO consultarRelease(String query) throws BusinessException {
		
		return this.converter(this.consultarReleaseDiscogs(query), true);
		
	}
	
	private DiscogsReleaseTO consultarReleaseDiscogs(String query) throws BusinessException {

		DiscogsSearchReleaseTO discogsSearchReleaseTO = this.listarRelease(query);

		if (discogsSearchReleaseTO == null) {
			throw new BusinessException("Não encontrou a query" + query + " no Discogs.");
		}

		if (discogsSearchReleaseTO.getResults() == null || discogsSearchReleaseTO.getResults().size() == 0) {
			throw new BusinessException("Não encontrou nenhum resultado para  query " + query + " no Discogs.");
		}

		DiscogsResultReleaseTO discogsResultReleaseTO = null;

		for (DiscogsResultReleaseTO to : discogsSearchReleaseTO.getResults()) {

			if ("release".equals(to.getType())) {

				discogsResultReleaseTO = to;

				break;

			}

		}

		if (discogsResultReleaseTO == null) {
			throw new BusinessException("Não encontrou o release da query " + query + " no Discogs.");
		}

		String idDiscogs = discogsResultReleaseTO.getId();

		DiscogsReleaseTO discogsReleaseTO = this.consultarIdRelease(idDiscogs);

		if (discogsReleaseTO == null) {
			throw new BusinessException("Sem retorno de informações do release da query " + query + " no Discogs.");
		}

		return discogsReleaseTO;

	}

	public AlbumTO consultarReleaseId(String idDiscogs) throws BusinessException {

		return this.converter(this.consultarIdRelease(idDiscogs), true);

	}

	private AlbumTO converter(DiscogsReleaseTO discogsReleaseTO, boolean carregaListaMusica) {

		AlbumTO albumTO = new AlbumTO();	
		
		albumTO.setAlbum(discogsReleaseTO.getTitle());

		albumTO.setAno(discogsReleaseTO.getYear());

		albumTO.setSelo(discogsReleaseTO.getLabels().get(0).getName());

		albumTO.setUrlImagem(discogsReleaseTO.getImages().get(0).getUri() + "?" + this.carregarKeySecret());
		
		List<ArtistaMusicaVO> listaArtistaMusicaTO = new ArrayList<ArtistaMusicaVO>();

		if (carregaListaMusica) {
			
			String artistaGeral = new String();
			
			for (DiscogsArtistsTO discogsArtistsTO : discogsReleaseTO.getArtists()) {
				
				artistaGeral = discogsArtistsTO.getName() + ", " + artistaGeral;  
				
			}
			
			if (!"".equals(artistaGeral)) {
				
				artistaGeral = artistaGeral.substring(0, artistaGeral.length() - 2);
				
			}

			for (DiscogsTracklistTO discogsTracklistTO : discogsReleaseTO.getTracklist()) {

				String musica = DecksUtil.ajustarArtistaMusica(discogsTracklistTO.getTitle());

				if (musica.indexOf(" - ") != -1) {

					ArtistaMusicaVO artistaMusicaTO = new ArtistaMusicaVO();

					String[] artistaMusica = musica.split(" - ");

					artistaMusicaTO.setArtista(artistaMusica[0]);

					artistaMusicaTO.setMusica(artistaMusica[1]);

					listaArtistaMusicaTO.add(artistaMusicaTO);

				} else {

					ArtistaMusicaVO artistaMusicaTO = new ArtistaMusicaVO();

					artistaMusicaTO.setArtista(artistaGeral);

					artistaMusicaTO.setMusica(musica);

					listaArtistaMusicaTO.add(artistaMusicaTO);

				}

			}

		}
		
		albumTO.setListaArtistaMusicaTO(listaArtistaMusicaTO);
		
		return albumTO;

	}
	
	/*
	public String consultarImagem(String url) {

		
		try {

			DiscogsReleaseTO discogsReleaseTO = consultarIdRelease(getIdToURL(url));
			
			if (discogsReleaseTO != null) {
				
				return discogsReleaseTO.getImages().get(0).getUri();
				
			}

		} catch (Exception e) {}

		return null;

	}
	*/

}

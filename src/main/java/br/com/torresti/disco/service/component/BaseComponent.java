package br.com.torresti.disco.service.component;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import br.com.torresti.spring.exception.BusinessException;

public class BaseComponent {
	
	public Document getJsoup(String valroURL) throws BusinessException {
		try {
			return Jsoup.connect(valroURL).get();
		} catch (Exception e) {
			throw new BusinessException(e);
		}

	}
	
}

package br.com.torresti.disco.service.dto.discogs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class ResultReleaseTO.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscogsResultReleaseTO {

	@JsonProperty(value = "thumb")
	private String thumb;

	@JsonProperty(value = "id")
	private String id;

	@JsonProperty(value = "type")
	private String type;

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}

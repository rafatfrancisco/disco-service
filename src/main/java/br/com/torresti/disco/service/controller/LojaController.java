package br.com.torresti.disco.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.torresti.disco.service.service.LojaService;
import br.com.torresti.musica.util.to.AlbumTO;
import br.com.torresti.musica.util.to.PesquisaAlbumTO;

@RestController
@RequestMapping(value = "/loja")
public class LojaController {

	@Autowired
	private LojaService lojaService;

	@GetMapping(value = "/consultar/query/{query}")
	public ResponseEntity<PesquisaAlbumTO> consultarQuery(@PathVariable String query) throws Exception {
		return new ResponseEntity<PesquisaAlbumTO>(lojaService.consultarQuery(query), HttpStatus.OK);
	}
	
	@GetMapping(value = "/consultar/query/{tipo}/{query}")
	public ResponseEntity<AlbumTO> consultarQuery(@PathVariable Integer tipo, @PathVariable String query) throws Exception {
		return new ResponseEntity<AlbumTO>(lojaService.consultarQuery(tipo, query), HttpStatus.OK);
	}
	
	@GetMapping(value = "/consultar/id/{tipo}/{id}")
	public ResponseEntity<AlbumTO> consultarId(@PathVariable Integer tipo, @PathVariable String id) throws Exception {
		return new ResponseEntity<AlbumTO>(lojaService.consultarId(tipo, id), HttpStatus.OK);
	}

}

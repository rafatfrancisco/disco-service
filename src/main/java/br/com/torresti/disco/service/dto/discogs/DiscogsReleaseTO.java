package br.com.torresti.disco.service.dto.discogs;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscogsReleaseTO {

	@JsonProperty(value = "id")
	private String id;

	@JsonProperty(value = "title")
	private String title;

	@JsonProperty(value = "year")
	private String year;

	@JsonProperty(value = "labels")
	private List<DiscogsLabelTO> labels;

	@JsonProperty(value = "artists")
	private List<DiscogsArtistsTO> artists;

	@JsonProperty(value = "tracklist")
	private List<DiscogsTracklistTO> tracklist;

	@JsonProperty(value = "images")
	private List<DiscogsImagesTO> images;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public List<DiscogsLabelTO> getLabels() {
		return labels;
	}

	public void setLabels(List<DiscogsLabelTO> labels) {
		this.labels = labels;
	}

	public List<DiscogsArtistsTO> getArtists() {
		return artists;
	}

	public void setArtists(List<DiscogsArtistsTO> artists) {
		this.artists = artists;
	}

	public List<DiscogsTracklistTO> getTracklist() {
		return tracklist;
	}

	public void setTracklist(List<DiscogsTracklistTO> tracklist) {
		this.tracklist = tracklist;
	}

	public List<DiscogsImagesTO> getImages() {
		return images;
	}

	public void setImages(List<DiscogsImagesTO> images) {
		this.images = images;
	}

}

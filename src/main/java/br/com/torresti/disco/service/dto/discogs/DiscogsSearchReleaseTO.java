package br.com.torresti.disco.service.dto.discogs;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class SearchReleaseTO.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscogsSearchReleaseTO {

	/** The results. */
	@JsonProperty(value = "results")
	private List<DiscogsResultReleaseTO> results;

	/**
	 * Gets the results.
	 *
	 * @return the results
	 */
	public List<DiscogsResultReleaseTO> getResults() {
		return results;
	}

	/**
	 * Sets the results.
	 *
	 * @param results the new results
	 */
	public void setResults(List<DiscogsResultReleaseTO> results) {
		this.results = results;
	}

}

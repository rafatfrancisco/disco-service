package br.com.torresti.disco.service.component;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.util.DecksUtil;
import br.com.torresti.musica.util.HtmlUtil;
import br.com.torresti.musica.util.MusicaConstantes.TipoSite;
import br.com.torresti.musica.util.MusicaUtil;
import br.com.torresti.musica.util.StringUtil;
import br.com.torresti.musica.util.to.AlbumTO;
import br.com.torresti.musica.util.to.ArtistaMusicaVO;
import br.com.torresti.spring.exception.BusinessException;
import br.com.torresti.spring.util.ViewUtil;

@Component
public class DecksComponent extends BaseComponent {
	
	public AlbumTO consultarRelease(String query) throws BusinessException {

		String url = "https://www.decks.de/decks-sess/workfloor/search_db.php?such=" + query + "&wassuch=all&wosuch=vi&imageField.x=40&imageField.y=11";
		
		Document document =  this.getJsoup(url);
		ViewUtil.isNull(document, "Não encontrou a query  " + query + " na Decks");
		
		Elements elements = document.select(".cover1").select("a");
		ViewUtil.isNull(elements, "Não encontrou a query  " + query + " na Decks");
				
		for (Element resultado : elements) {
			
			String idDecks = resultado.attr("href").replace("/decks-sess/workfloor/lists/findTrack.php?code=", "");
			
			if (!StringUtil.isEmpty(idDecks)) {
				
				return this.consultarReleaseId(idDecks);

			}
			
		}
				
		throw new BusinessException("Erro comsulta no Decks.");
		
	}
	
	public AlbumTO consultarReleaseId(String id) throws BusinessException {
		
		String url = MusicaUtil.obterUrlToId(TipoSite.Decks.getTipo(), id);
		
		Document document = this.getJsoup(url);
		
		String artistaPrincipal = this.obterArtistaPrincipal(document, null);
		
		AlbumTO albumTO = new AlbumTO();
		albumTO.setAlbum(this.obterAlbum(document, null));
		albumTO.setAno(this.obterAno(document, null));
		albumTO.setSelo(this.obterSelo(document, null));
		albumTO.setUrlImagem(this.obterUrlImagem(document, null));
		albumTO.setListaArtistaMusicaTO(this.obterListaMusica(artistaPrincipal, document.select(".OneListen")));
		
		return albumTO;
		
	}
	
	private String obterArtistaPrincipal(Document document, Elements elements) throws BusinessException {
		return DecksUtil.ajustarArtista(HtmlUtil.removeTag((document.select(".detail_artist").first().html())));
	}
	
	private String obterAlbum(Document document, Elements elements) throws BusinessException {
		return StringUtil.ajustarTextoReleaseTO(HtmlUtil.removeTag(HtmlUtil.removeTag(document.select(".detail_titel").first().html()),"(",")"));
	}
	
	private String obterAno(Document document, Elements elements) throws BusinessException {
		return document.select(".infodateval").first().html().substring(6, 10);
	}
	
	private String obterSelo(Document document, Elements elements) throws BusinessException {
		Element elementSelo = document.select(".detail_label").first();
		if (elementSelo.html().contains("</h1>")) {
			return HtmlUtil.removeValor(HtmlUtil.removeValor(elementSelo.select("h1").html(), " / "), "-");
		} else {
			return HtmlUtil.removeValor(HtmlUtil.removeValor(elementSelo.html(), " / "), "-");
		}
	}
	
	private String obterUrlImagem(Document document, Elements elements) throws BusinessException {
		Element elementImagem = document.select(".bigCoverDetail").first().children().first();
		if (elementImagem != null) {
			return elementImagem.attr("src");
		}
		return "http://www.pattonmad.com/Vinyl%20&%20CD/Images/Vinyl/12/MidlifeCrisis12UKPurpleVinylPromoB.jpg";
	}
	
	private List<ArtistaMusicaVO> obterListaMusica(String artistaPrincipal, Elements elements) throws BusinessException {
		
		List<ArtistaMusicaVO> listaArtistaMusicaTO = new ArrayList<ArtistaMusicaVO>();
		
		for (Element elementMusica : elements) {
			
			String musica = DecksUtil.ajustarArtistaMusica(elementMusica.select(".TrackNametxt").html());
			
			if (!"Please Login For Sounds".equals(musica)) {
			
				if (musica.indexOf(" - ") != -1) {
					
					ArtistaMusicaVO artistaMusicaTO = new ArtistaMusicaVO();
					
					String[] artistaMusica = musica.split(" - "); 
					
					artistaMusicaTO.setArtista(artistaMusica[0]);
					
					artistaMusicaTO.setMusica(artistaMusica[1]);
					
					listaArtistaMusicaTO.add(artistaMusicaTO);
															
				} else {
					
					ArtistaMusicaVO artistaMusicaTO = new ArtistaMusicaVO();
					
					artistaMusicaTO.setArtista(artistaPrincipal);
					
					artistaMusicaTO.setMusica(musica);
					
					listaArtistaMusicaTO.add(artistaMusicaTO);			
					
				}
				
			}
			
		}
		
		return listaArtistaMusicaTO;
		
	}
	
}

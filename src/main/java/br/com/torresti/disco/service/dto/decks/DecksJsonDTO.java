package br.com.torresti.disco.service.dto.decks;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DecksJsonDTO {

	@JsonProperty(value = "image")
	private String image;

	@JsonProperty(value = "releaseDate")
	private String releaseDate;

	@JsonProperty(value = "brand")
	private DecksBrandJsonDTO brand;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public DecksBrandJsonDTO getBrand() {
		return brand;
	}

	public void setBrand(DecksBrandJsonDTO brand) {
		this.brand = brand;
	}

}

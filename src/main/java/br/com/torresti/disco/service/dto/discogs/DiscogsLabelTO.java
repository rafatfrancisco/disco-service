package br.com.torresti.disco.service.dto.discogs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class LabelTO.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscogsLabelTO {

	/** The name. */
	@JsonProperty(value = "name")
	private String name;

	/** The catno. */
	@JsonProperty(value = "catno")
	private String catno;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the catno.
	 *
	 * @return the catno
	 */
	public String getCatno() {
		return catno;
	}

	/**
	 * Sets the catno.
	 *
	 * @param catno the new catno
	 */
	public void setCatno(String catno) {
		this.catno = catno;
	}

}

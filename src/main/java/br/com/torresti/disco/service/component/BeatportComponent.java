package br.com.torresti.disco.service.component;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.torresti.disco.service.dto.beatport.BeatportJsonDTO;
import br.com.torresti.musica.util.StringUtil;
import br.com.torresti.musica.util.to.AlbumTO;
import br.com.torresti.spring.exception.BusinessException;

@Component
public class BeatportComponent extends BaseComponent {

	private AlbumTO consultarRelease(String nameIdBeatport, String idBeatport) throws BusinessException {
		
		String url = "https://www.beatport.com/release/" + nameIdBeatport + "/" + idBeatport;
		
		Document document = this.getJsoup(url);
		
		if (document != null) {
			
			AlbumTO albumTO = new AlbumTO();
			
			Elements lista = document.select("script");
			
			for (Element element : lista) {
				
				String json = element.html();
				
				if (json.indexOf("window.ProductDetail =") != -1) {
					
					json = json.replaceAll("window.ProductDetail =", "");
					
					Gson gson = new Gson();
					
					BeatportJsonDTO beatportJsonDTO = gson.fromJson(json,new TypeToken<BeatportJsonDTO>() {}.getType());
					
					albumTO.setAlbum(beatportJsonDTO.getName());
					
					albumTO.setAno(beatportJsonDTO.getDate().getReleased().substring(0, 4));
					
					albumTO.setSelo(beatportJsonDTO.getLabel().getName());
					
					albumTO.setUrlImagem(beatportJsonDTO.getImages().getLarge().getUrl());
					
					return albumTO;
					
				}
				
			}
			
		}
		
		return null;
		
	}
	
	public AlbumTO consultarRelease(String query) throws BusinessException {
		
		String url = "https://www.beatport.com/search?q=" + query + "";
		
		Document document = this.getJsoup(url);
		
		Elements elements = document.select(".release-artwork-parent").select("a");
		
		if (elements == null || elements.isEmpty()) { 
			
			elements = document.select(".buk-track-artwork-parent").select("a");
		
			if (elements == null || elements.isEmpty()) { 
			
				throw new BusinessException("Não encontrou a query " + query + " no Beatport.");
				
			}
			
		}
				
		for (Element resultado : elements) {
			
			String[] listaId = resultado.attr("href").replaceAll("/release/", "").split("/");
			
			String nameIdBeatport = listaId[0];
			
			String idBeatport = listaId[1];
			
			if (!StringUtil.isEmpty(nameIdBeatport) && !StringUtil.isEmpty(idBeatport)) {
				
				AlbumTO albumTO = consultarRelease(nameIdBeatport, idBeatport);
				
				if (albumTO != null) {
					
					return albumTO;
					
				} else {
					
					throw new BusinessException("Não encontrou o release com as informações de nameIdBeatport " + nameIdBeatport + " e idBeatport " + idBeatport + " no Beatport.");
					
				}
				
				
			} else {
				
				throw new BusinessException("Não encontrou as informações de nameIdBeatport " + nameIdBeatport + " e idBeatport " + idBeatport + " no Beatport.");
				
			}
			
		}
				
		throw new BusinessException("Erro comsulta no Beatport.");
		
	}
	
}

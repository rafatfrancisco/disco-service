package br.com.torresti.disco.service.dto.beatport;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeatportDateJsonDTO {

	@JsonProperty(value = "released")
	private String released;

	public String getReleased() {
		return released;
	}

	public void setReleased(String released) {
		this.released = released;
	}
	
}

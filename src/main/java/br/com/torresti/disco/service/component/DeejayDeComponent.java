package br.com.torresti.disco.service.component;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import br.com.torresti.musica.util.ArtistaMusicaUtil;
import br.com.torresti.musica.util.to.AlbumTO;
import br.com.torresti.musica.util.to.ArtistaMusicaVO;
import br.com.torresti.spring.exception.BusinessException;
import br.com.torresti.spring.util.ViewUtil;

@Component
public class DeejayDeComponent extends BaseComponent {
	
	public AlbumTO consultarRelease(String query) throws BusinessException {
		
		String url = "https://www.deejay.de/content.php?param=" + query;
		
		Document document = this.getJsoup(url);
		ViewUtil.isNull(document, "Não encontrou a query  " + query + " na DeejayDe");
		
		Elements elements = document.select(".product");
		ViewUtil.isNull(elements, "Não encontrou a query  " + query + " na DeejayDe");

		for (Element resultado : elements) {
			
			String id = resultado.select(".img").first().select("a").get(1).attr("href").substring(1);
			
			return this.consultarReleaseId(id);
			
		}
				
		throw new BusinessException("Erro consulta no DeejayDe");
		
	}
	
	public AlbumTO consultarReleaseId(String id) throws BusinessException {
		
		String url = "https://www.deejay.de/content.php?param=" + id;
		
		Document document = this.getJsoup(url);
				
		Elements elements = document.select(".single_product");
		
		String artistaPrincipal = this.obterArtistaPrincipal(document, elements);
		
		AlbumTO albumTO = new AlbumTO();
		albumTO.setAlbum(this.obterAlbum(document, elements));
		albumTO.setAno(this.obterAno(document, elements));
		albumTO.setSelo(this.obterSelo(document, elements));
		albumTO.setUrlImagem(this.obterUrlImagem(document, elements));
		albumTO.setListaArtistaMusicaTO(this.obterListaMusica(artistaPrincipal, elements));
		
		return albumTO;
		
	}

	private String obterArtistaPrincipal(Document document, Elements elements) throws BusinessException {
		return ArtistaMusicaUtil.ajustarArtista(elements.select(".artist").first().select("h1").text());
	}
	
	private String obterAlbum(Document document, Elements elements) throws BusinessException {
		return ArtistaMusicaUtil.ajustarAlbum(elements.select(".title").first().select("h1").text());
	}
	
	private String obterAno(Document document, Elements elements) throws BusinessException {
		return elements.select(".date").first().text().replace("<b>Release : </b> ", "").replace("Release : ", "").substring(6, 10);
	}
	
	private String obterSelo(Document document, Elements elements) throws BusinessException {
		return ArtistaMusicaUtil.ajustarSelo(elements.select(".label").first().select("h3").text());
	}
	
	private String obterUrlImagem(Document document, Elements elements) throws BusinessException {
		return "https://www.deejay.de" + elements.select(".img").first().select("a").first().attr("href");
	}
	
	private List<ArtistaMusicaVO> obterListaMusica(String artistaPrincipal, Elements elements) throws BusinessException {
		
		List<ArtistaMusicaVO> listaArtistaMusicaTO = new ArrayList<>();
		
		for (Element element : elements.select(".playtrack").first().select("li")) {
			
			String musica = element.select(".track").select("h5").text();
			
			listaArtistaMusicaTO.add(ArtistaMusicaUtil.obterArtistaMusicaVO(artistaPrincipal, musica));
			
		}
		
		return listaArtistaMusicaTO;
		
	}
	
	/*
	public String obterArtistaPrincipal(Document document, Elements elements) throws BusinessException {
		return null;
	}
	
	public String obterAlbum(Document document, Elements elements) throws BusinessException {
		return null;
	}
	
	public String obterAno(Document document, Elements elements) throws BusinessException {
		return null;
	}
	
	public String obterSelo(Document document, Elements elements) throws BusinessException {
		return null;
	}
	
	public String obterUrlImagem(Document document, Elements elements) throws BusinessException {
		return null;
	}
	
	public List<ArtistaMusicaVO> obterListaMusica(String artistaPrincipal, Elements elements) throws BusinessException {
		return null;
	}
	*/

}
